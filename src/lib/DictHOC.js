import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import DictContext from './DictContext';
import { createPhrase } from './localizeHelper';

class DictPhraseCreator {
    constructor(phrases) {
        this.phrases = phrases;
    }

    t(key, replace = {}) {
        return createPhrase.get(key, this.phrases, replace);
    }

    p() {
        return this.phrases;
    }
}

const propertyBlacklist = [
    'WrappedComponent',
    'childContextTypes',
    'contextTypes',
    'defaultProps',
    'displayName',
    'getDefaultProps',
    'getDerivedStateFromProps',
    'mixins',
    'propTypes',
    'type',
    'name',
    'length',
    'prototype',
    'caller',
    'callee',
    'arguments',
    'arity'
];

const DictHOC = (WrappedComponent, scope) => {
    class Dict extends PureComponent {
        constructor(props) {
            super(props);
            this.ref = null;
        }

        componentDidMount() {
            /*
                in case you need a ref to the WrappedComponent
                create this.myRefs and pass onRef property like so:
                onRef={item => { this.myRefs[myKey] = item }}
            */
            if (this.props.onRef) {
                this.props.onRef(this.ref);
            }
        }

        render() {
            return (
                <DictContext.Consumer>
                    {contextPhrases => {
                        const phrases = scope && contextPhrases[scope]
                            ? contextPhrases[scope]
                            : contextPhrases;

                        const dict = new DictPhraseCreator(phrases);
                        return this.props.onRef
                            ? <WrappedComponent ref={node => { this.ref = node; }} {...this.props} dict={dict} />
                            : <WrappedComponent {...this.props} dict={dict} />;
                    }}
                </DictContext.Consumer>
            );
        }
    }

    (() => {
        Object.getOwnPropertyNames(WrappedComponent).forEach(prop => {
            if (!propertyBlacklist.includes(prop)) {
                Dict[prop] = WrappedComponent[prop];
            }
        });
    })();

    Dict.propTypes = {
        onRef: PropTypes.func
    };

    return Dict;
};

export default DictHOC;
