import React from 'react';
import { render } from 'react-dom';
import TestComponent from './TestComponent';
import { DictContext } from '../../lib';

function Demo() {
    const textObject = {
        myText: 'we have ${smart_count} word about ${replacement} |||| we have ${smart_count} words about ${replacement}'
    };
    return (
        <div>
            <h1>Sixt Dict Demo</h1>
            <DictContext.Provider value={textObject}>
                <TestComponent />
            </DictContext.Provider>
        </div>
    );
}

render(<Demo />, document.getElementById("app"));
