import React from 'react';
import Dict from '../../lib';

const TestComponent = props => {
    const count = Math.floor(Math.random() * 5) + 1;
    return (
        <div>
            <p>Here comes the text passed by Dict</p>
            {props.dict.t('myText', {
                smart_count: count,
                replacement: 'sunhine'
            })}
        </div>
    );
};

export default Dict(TestComponent, false);
